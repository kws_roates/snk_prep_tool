﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace snk_orange_prep
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;


            string macro = @"Multilen_notag.bas";
            string multilen = File.ReadAllText(macro);

            List<string> file_list = new List<string>();

            while (true)
            {

                Console.WriteLine("Copy and paste file");
                string file = Console.ReadLine();
                file = file.Replace("\"", "");

                file_list.Clear();



                if (file.ToLower().EndsWith(".xlsx") || file.ToLower().EndsWith(".xls") || file.ToLower().EndsWith(".xlsm"))
                {
                    Console.WriteLine("Processing this is file");
                    
                    file_list.Add(file);
                    
                    
                }
                else
                {
                    Console.WriteLine("Listing files");
                    string[] files = Directory.GetFiles(file, "*.xls*");

                    foreach (string filex in files)
                    {
                        if (new FileInfo(filex).Name.StartsWith("~$") == false)
                        {
                            Console.WriteLine(filex);
                            file_list.Add(filex);
                        }
                            
                    }

                }
                




                bool choice_made = false;
                string option = string.Empty;

                while (choice_made == false)
                {
                    Console.WriteLine(
                        "Options:" +
                        "\n0 - Clean file of Janky things \\r and \\t (use anytime)" +
                        "\n1 - Add missing columns to original src file (use on Source file)" +
                        "\n2 - Convert \\n to <br> (use on Prep File)" +
                        "\n3 - Convert <br> to \\n (use on Prep File)" +
                        "\n4 - Add multi-len Macro (use on Prep File)" +
                        "\n5 - Split file into tabs (use on Prep File)" +
                        "\n6 - Add missing columns to original src file (use on Source file POST Translation!!!)"

                        );

                    option = Console.ReadLine();
                    switch (option)
                    {
                        case "0":
                            choice_made = true;
                            break;
                        case "1":
                            choice_made = true;
                            break;
                        case "2":
                            choice_made = true;
                            break;
                        case "3":
                            choice_made = true;
                            break;
                        case "4":
                            choice_made = true;
                            break;
                        case "5":
                            choice_made = true;
                            break;
                        case "6":
                            choice_made = true;
                            break;
                        default:
                            break;
                    }
                }

                if(option == "5")
                {
                    foreach (string filen in file_list)
                    {
                        GenerateTabs(filen);
                    }
                    
                }
                else
                {
                    foreach (string filen in file_list)
                    {
                        ExcelPackage excel = new ExcelPackage(new FileInfo(filen));

                        int count = 0;
                        if (Convert.ToInt32(option) == 4)
                        {
                            excel.Workbook.CreateVBAProject();
                            var module = excel.Workbook.VbaProject.Modules.AddModule("Module1");
                            module.Code = multilen;
                        }


                        foreach (ExcelWorksheet ws in excel.Workbook.Worksheets)
                        {
                            //clean file of janky things
                            if (option == "0")
                            {
                                
                                for (int row = 2; row < ws.Dimension.Rows + 1; row++)
                                {
                                    for (int col = 1; col < ws.Dimension.Columns + 1; col++)
                                    {
                                        if (ws.Cells[row, col].Value != null)
                                        {
                                            string sentence = ws.Cells[row, col].Text;
                                            if (sentence.Contains("\t"))
                                            {
                                                sentence = sentence.Replace("\t", " ");
                                                Console.WriteLine($"[{row},{col}] = {sentence}");
                                                ws.Cells[row, col].Value = sentence.TrimEnd();
                                            }
                                            if (sentence.Contains("\r"))
                                            {
                                                sentence = sentence.Replace("\r", "");
                                                Console.WriteLine($"[{row},{col}] = {sentence}");
                                                ws.Cells[row, col].Value = sentence;
                                            }
                                        }
                                    }
                                }
                            }

                            //add missing columns to src file
                            if (option == "1" || option =="6")
                            {
                                if (ws.Hidden.ToString() == "Visible")
                                {
                                    
                                    if (ws.Name == "Common")
                                    {
                                        ws.Cells[1, ColumnLetterToColumnIndex("C")].Value = "ja-JP";
                                    }
                                    
                                    
                                    if(
                                        ws.Name == "Stadia" ||
                                        ws.Name == "Xbox" ||
                                        ws.Name == "ポリシー・規約" ||
                                        ws.Name == "ストーリー" ||
                                        ws.Name == "エンディング（ロスター）" ||
                                        ws.Name == "エンディング（DLC）" ||
                                        ws.Name == "リザルトメッセージ（ロスター）" ||
                                        ws.Name == "リザルトメッセージ（DLC）" ||
                                        ws.Name == "インゲームデモ（ロスター）" ||
                                        ws.Name == "インゲームデモ（DLC）" ||
                                        ws.Name == "技名（ロスター）" ||
                                        ws.Name == "技名（DLC）" ||
                                        ws.Name == "CPU登場"
                                        )
                                    {
                                        ws.Cells[1, ColumnLetterToColumnIndex("R")].Value = "ar-AR";
                                        ws.Cells[1, ColumnLetterToColumnIndex("S")].Value = "ru-RU";
                                        ws.Cells[1, ColumnLetterToColumnIndex("T")].Value = "th-TH";
                                    }

                                    if (option == "1")
                                    {
                                        ws.Cells[1, ColumnLetterToColumnIndex("U")].Value = "Western Max Char limit";
                                        ws.Cells[1, ColumnLetterToColumnIndex("V")].Value = "Eastern Max Char limit";
                                        ws.Cells[1, ColumnLetterToColumnIndex("W")].Value = "Western Char limit";
                                        ws.Cells[1, ColumnLetterToColumnIndex("X")].Value = "Eastern Char limit";
                                        ws.Cells[1, ColumnLetterToColumnIndex("Y")].Value = "Lines";

                                        for (int row = 2; row < ws.Dimension.Rows + 1; row++)
                                        {
                                            //western char limit
                                            ws.Cells[row, ColumnLetterToColumnIndex("U")].Value = 240;
                                            ws.Cells[row, ColumnLetterToColumnIndex("U")].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                                            //eastern char limit 
                                            ws.Cells[row, ColumnLetterToColumnIndex("V")].Value = 120;
                                            ws.Cells[row, ColumnLetterToColumnIndex("V")].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                                            //western chars per row
                                            ws.Cells[row, ColumnLetterToColumnIndex("W")].Value = 80;
                                            ws.Cells[row, ColumnLetterToColumnIndex("W")].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                                            //eastern chars per row
                                            ws.Cells[row, ColumnLetterToColumnIndex("X")].Value = 40;
                                            ws.Cells[row, ColumnLetterToColumnIndex("X")].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                                            //max number of lines
                                            ws.Cells[row, ColumnLetterToColumnIndex("Y")].Value = 3;
                                            ws.Cells[row, ColumnLetterToColumnIndex("Y")].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                                        }
                                    }
                                    
                                    
                                    
                                }  

                            }

                            //convert \n to <br> to be done on prep file
                            if (option == "2")
                            {
                                for (int row = 2; row < ws.Dimension.Rows + 1; row++)
                                {
                                    for (int col = 1; col < ws.Dimension.Columns + 1; col++)
                                    {
                                        if (ws.Cells[row, col].Value != null)
                                        {
                                            string sentence = ws.Cells[row, col].Text;
                                            if (sentence.Contains("\n"))
                                            {
                                                string[] words = sentence.Split('\n');

                                                sentence = sentence.Replace("\n", "<br>");
                                                //sentence = sentence.Replace("\n", "\\n");

                                                ws.Cells[row, col].Value = sentence;
                                                count = count + words.Count() - 1;

                                                Console.WriteLine($"[{row},{col}] {words.Count() - 1} :: {count} = {sentence}");
                                            }
                                        }
                                    }
                                }
                            }

                            //convert <br> to \n on prep file before returning 
                            if (option == "3")
                            {
                                for (int row = 2; row < ws.Dimension.Rows + 1; row++)
                                {
                                    for (int col = 1; col < ws.Dimension.Columns + 1; col++)
                                    {
                                        if (ws.Cells[row, col].Value != null)
                                        {
                                            string sentence = ws.Cells[row, col].Text;
                                            if (sentence.Contains("<br>") || sentence.Contains("<BR>"))
                                            {
                                                sentence = sentence.Replace("<br>", "\n");
                                                sentence = sentence.Replace("<BR>", "\n");
                                                Console.WriteLine($"[{row},{col}] = {sentence}");
                                                ws.Cells[row, col].Value = sentence;
                                            }
                                        }
                                    }
                                }
                            }
                            if (option == "4")
                            {
                                int j = ColumnLetterToColumnIndex("G");
                                //make the header
                                for (int k = ColumnLetterToColumnIndex("AP"); k <= ColumnLetterToColumnIndex("AP") + 12; k++)
                                {
                                    ws.Cells[1, k].Value = ws.Cells[1, j].Value.ToString();
                                    j = j + 2;
                                }

                                for (int row = 2; row < ws.Dimension.Rows + 1; row++)
                                {
                                    j = ColumnLetterToColumnIndex("G");

                                    for (int i = ColumnLetterToColumnIndex("AP"); i <= ColumnLetterToColumnIndex("AP") + 12; i++)
                                    {
                                        ws.Cells[row, ColumnLetterToColumnIndex("AK")].Value = 240;
                                        ws.Cells[row, ColumnLetterToColumnIndex("AL")].Value = 120;
                                        ws.Cells[row, ColumnLetterToColumnIndex("AM")].Value = 80;
                                        ws.Cells[row, ColumnLetterToColumnIndex("AN")].Value = 40;
                                        ws.Cells[row, ColumnLetterToColumnIndex("AO")].Value = 3;

                                        switch (j)
                                        {
                                            case 21:
                                            case 23:
                                            case 25:
                                            case 31:
                                                ws.Cells[row, i].Formula = $"=MULTILEN({ColumnIndexToColumnLetter(j)}{row},AN{row},AO{row})";
                                                break;
                                            default:
                                                ws.Cells[row, i].Formula = $"=MULTILEN({ColumnIndexToColumnLetter(j)}{row},AM{row},AO{row})";
                                                break;

                                        }


                                        j = j + 2;
                                    }




                                }
                            }


                        }

                        if (Convert.ToInt32(option) == 0)
                        {
                            excel.SaveAs(new FileInfo(filen.Replace(".xls", "_cleaned.xls")));
                        }
                        if (Convert.ToInt32(option) == 1)
                        {
                            excel.SaveAs(new FileInfo(filen.Replace(".xls", "_added_columns.xls")));
                        }
                        if (Convert.ToInt32(option) == 2)
                        {
                            excel.SaveAs(new FileInfo(filen.Replace(".xls", "_relaced_newline_with_br.xls")));
                        }
                        if (Convert.ToInt32(option) == 3)
                        {
                            excel.SaveAs(new FileInfo(filen.Replace(".xls", "_relaced_br_with_newline.xls")));
                        }
                        if (Convert.ToInt32(option) == 4)
                        {
                            excel.SaveAs(new FileInfo(filen.Replace(".xlsx", "_with_multilen_macro.xlsm")));
                        }
                        if (Convert.ToInt32(option) == 6)
                        {
                            excel.SaveAs(new FileInfo(filen.Replace(".xls", "_post_prep.xls")));
                        }

                        Console.WriteLine(count);
                    }
                }
                

                

                /*
                string excel_origin = @"D:\KWS\Rohit\2021-01-11\Wk7_Liveupdate.xlsx";
                string excel_context_path = @"D:\KWS\Rohit\2021-01-11\mood";


                //build_list of character with moods
                string[] files = Directory.GetFiles(excel_context_path, "*.xlsx");

                List<DB> speaker_and_moods = new List<DB>();

                foreach (string file in files)
                {
                    ExcelPackage excel_context = new ExcelPackage(new FileInfo(file));

                    foreach (ExcelWorksheet ws in excel_context.Workbook.Worksheets)
                    {
                        for (int row = 3; row < ws.Dimension.Rows+1; row++)
                        {
                            if (ws.Cells[row, ColumnLetterToColumnIndex("B")].Value != null)
                            {
                                if(ws.Cells[row, ColumnLetterToColumnIndex("B")].Value.ToString() != "")
                                {
                                    string id = ws.Cells[row, ColumnLetterToColumnIndex("B")].Value.ToString();
                                    string speaker = string.Empty;
                                    string mood = string.Empty;
                                    //speaker
                                    if (ws.Cells[row, ColumnLetterToColumnIndex("G")].Value == null)
                                    {
                                        speaker = "";
                                    }
                                    else
                                    {
                                        speaker = ws.Cells[row, ColumnLetterToColumnIndex("G")].Value.ToString();
                                    }

                                    //mood
                                    if (ws.Cells[row, ColumnLetterToColumnIndex("I")].Value == null)
                                    {
                                        mood = "";
                                    }
                                    else
                                    {
                                        mood = ws.Cells[row, ColumnLetterToColumnIndex("I")].Value.ToString();
                                    }

                                    Console.WriteLine($"{id}.text {speaker} {mood}");
                                    speaker_and_moods.Add(new DB($"{id}.text", speaker, mood));

                                }
                            }
                        }
                    }

                }




                ExcelPackage excel_original = new ExcelPackage(new FileInfo(excel_origin));

                ExcelWorksheet wsx = excel_original.Workbook.Worksheets[0];

                for (int row = 3; row < wsx.Dimension.Rows+1; row++)
                {
                    if (wsx.Cells[row, 1].Value != null)
                    {
                        foreach (DB item in speaker_and_moods.Where(x=>x.Id == wsx.Cells[row, 1].Value.ToString()))
                        {
                            wsx.Cells[row, ColumnLetterToColumnIndex("I")].Value = item.Person;
                            wsx.Cells[row, ColumnLetterToColumnIndex("J")].Value = item.Mood;
                        }
                    }
                }


                excel_original.SaveAs(new FileInfo(excel_origin.Replace(".xlsx", "_prepared.xlsx")));
                */
                Console.WriteLine("done");
            }




        }

        public static void GenerateTabs(string file)
        {
            ExcelPackage excel_src = new ExcelPackage(new FileInfo(file));
            ExcelWorksheet ws_src = excel_src.Workbook.Worksheets[0];

            ExcelPackage excel_out = new ExcelPackage();
            ExcelWorksheet ws_out = excel_out.Workbook.Worksheets.Add("Sheet1");

            //initial tab
            string current_tab = string.Empty;


            int tab = 1;
            int rown = 1;

            for (int row = 2; row < ws_src.Dimension.Rows+1; row++)
            {

                if (ws_src.Cells[row, 2].Value != null)
                {
                    string new_tab = ws_src.Cells[row, 2].Value.ToString();

                    if (current_tab != new_tab)
                    {
                        
                        if (row != 2)
                        {
                            //save the current tab
                            Console.WriteLine($"{new FileInfo(file).Name.Replace(".xlsx","")}_tab_{current_tab}.xlsx");
                            excel_out.SaveAs(new FileInfo(file.Replace(".xlsx", $"_[{current_tab}]_2.0.xlsx")));
                            tab++;
                            rown = 1;
                            excel_out = new ExcelPackage();
                            ws_out = excel_out.Workbook.Worksheets.Add("Sheet1");
                            //copy the header
                            for (int i = 1; i < ws_src.Dimension.Columns+1; i++)
                            {
                                ws_out.Cells[rown, i].Value = ws_src.Cells[1, i].Value.ToString();
                            }
                            rown++;
                            for (int col = 1; col < ws_src.Dimension.Columns + 1; col++)
                            {
                                if (ws_src.Cells[row, col].Value != null)
                                {
                                    ws_out.Cells[rown, col].Value = ws_src.Cells[row, col].Value.ToString();

                                    if (ws_src.Cells[row, col].Style.Fill.PatternType.ToString() != "None")
                                    {
                                        string background_color = ws_src.Cells[row, col].Style.Fill.BackgroundColor.LookupColor();
                                        ws_out.Cells[rown, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        ws_out.Cells[rown, col].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(background_color));
                                    }

                                }

                            }
                            rown++;

                            //current_tab = new_tab;
                        }
                        else
                        {
                            for (int i = 1; i < ws_src.Dimension.Columns + 1; i++)
                            {
                                ws_out.Cells[rown, i].Value = ws_src.Cells[1, i].Value.ToString();


                            }
                            rown++;

                            for (int col = 1; col < ws_src.Dimension.Columns + 1; col++)
                            {
                                if (ws_src.Cells[row, col].Value != null)
                                {
                                    ws_out.Cells[rown, col].Value = ws_src.Cells[row, col].Value.ToString();

                                    if (ws_src.Cells[row, col].Style.Fill.PatternType.ToString() != "None")
                                    {
                                        string background_color = ws_src.Cells[row, col].Style.Fill.BackgroundColor.LookupColor();
                                        ws_out.Cells[rown, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        ws_out.Cells[rown, col].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(background_color));
                                    }

                                }

                            }
                            rown++;

                        }

                        current_tab = new_tab;
                    }
                    else
                    {
                        for (int col = 1; col < ws_src.Dimension.Columns+1; col++)
                        {
                            if (ws_src.Cells[row, col].Value != null)
                            {
                                ws_out.Cells[rown, col].Value = ws_src.Cells[row, col].Value.ToString();
                                if (ws_src.Cells[row, col].Style.Fill.PatternType.ToString() != "None")
                                {
                                    string background_color = ws_src.Cells[row, col].Style.Fill.BackgroundColor.LookupColor();
                                    ws_out.Cells[rown, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    ws_out.Cells[rown, col].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(background_color));
                                }
                            }
                            
                        }
                        rown++;
                    }


                }
            }

            excel_out.SaveAs(new FileInfo(file.Replace(".xlsx", $"_[{current_tab}]_2.0.xlsx")));




        }

        public static int ColumnLetterToColumnIndex(string columnLetter)
        {
            columnLetter = columnLetter.ToUpper();
            int sum = 0;

            for (int i = 0; i < columnLetter.Length; i++)
            {
                sum *= 26;
                sum += (columnLetter[i] - 'A' + 1);
            }
            return sum;
        }

        public static string ColumnIndexToColumnLetter(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = string.Empty;
            int modulo;

            if (dividend == -1)
            {
                columnName = "N/A";
            }
            else
            {
                while (dividend > 0)
                {
                    modulo = (dividend - 1) % 26;
                    columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                    dividend = (dividend - modulo) / 26;
                }
            }

            return columnName;
        }

        public class DB
        {
            private string id;
            private string person;
            private string mood;

            public DB(string id, string person, string mood)
            {
                this.id = id;
                this.person = person;
                this.mood = mood;

            }

            public string Id
            {
                get { return id; }
            }

            public string Person
            {
                get { return person; }
            }

            public string Mood
            {
                get { return mood; }
            }

        }
    }
}

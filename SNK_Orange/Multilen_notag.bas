Function MULTILEN(InText As String, Optional MaxLen As Integer = -1, Optional MaxLines As Integer = -1) As String
    
    InText = Replace(InText, Chr(13), "")
    InText = StrNoCode(InText)
    LineArr = Split(InText, Chr(10))
    Dim i As Integer
    Dim UnicodeValue As Long
    MULTILEN = ""
    
    For j = 0 To UBound(LineArr)
        LenCell = 0
        If MULTILEN <> "" Then
            MULTILEN = MULTILEN + Chr(10)
        End If
        lenchar = 0
        StrCurr = LineArr(j)
        For i = 1 To Len(StrCurr)
            UnicodeValue = CharToUnicode(Mid(StrCurr, i, Len(StrCurr)))
            lenchar = lenchar + 1
            ' if u'\u2f00' <= c <= u'\u9fff':
            If UnicodeValue >= 12032 And UnicodeValue <= 66000 Then
                'lenchar = lenchar + 1
            End If
            
        Next
        MULTILEN = MULTILEN + Trim(Str(lenchar))
        If MaxLines >= 0 And j + 1 > MaxLines Then
            MULTILEN = MULTILEN + " EXTRA LINE!"
        ElseIf MaxLen >= 0 And lenchar > MaxLen Then
            MULTILEN = MULTILEN + " TOO LONG!"
        End If
    Next
    If MULTILEN = "" Then
        MULTILEN = "0"
    End If
End Function
Function CharToUnicode(strChar As String)
    Dim lngUnicode As Long

    lngUnicode = AscW(strChar)

    If lngUnicode < 0 Then
        lngUnicode = 65536 + lngUnicode
    End If

    CharToUnicode = lngUnicode
End Function

Function WRITEBYTE(InText As String, Optional MaxLen As Integer = -1, Optional MaxLines As Integer = -1) As String
    
    InText = Replace(InText, Chr(13), "")
    Dim abData() As Byte
    Dim i As Integer
    WRITEBYTE = ""
    
    abData = StrConv(InText, vbFromUnicode)
    

    For i = 1 To Len(InText)
        WRITEBYTE = WRITEBYTE + Trim(Str(CharToUnicode(Mid(InText, i, Len(InText))))) & " "
    Next

End Function


Function StrNoCode(InText As String) As String
        Dim regEx As Object
        Set regEx = CreateObject("vbscript.regexp")
        
        With regEx
            .Global = True
            .MultiLine = True
            .IgnoreCase = True
            .Pattern = "<.*?>"
        End With
    
'        InText = regEx.Replace(InText, "")
'        regEx.Pattern = "(\\n)"
'        InText = regEx.Replace(InText, "")
'        regEx.Pattern = "(#)"
'        InText = regEx.Replace(InText, "")
'        regEx.Pattern = ("ss..\(neco\)")
        StrNoCode = regEx.Replace(InText, "")

End Function


